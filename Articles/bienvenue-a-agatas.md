# Bienvenue à AGATAs! 
*Par Gaetan Guyet*

C’est avec une certaine émotion que nous publions ces lignes. Elles marquent l’arrivée, certes encore balbutiante, d’AGATAs sur la toile !  Si vous êtes ici, c’est sans doute parce qu’on vous a déjà parlé du projet. Mais si ce n’est pas le cas, ou si notre pitch de présentation était un échec total, ce premier article va vous (re)expliquer le qui du quoi du comment, et surtout, ce que vous pouvez y faire. Bienvenue à AGATAs ! 

#### AGATAs, c'est qui, c'est quoi ? 
L'histoire d'AGATAs a commencé dans les têtes de Vincent Espagne et moi-même, Gaëtan Guyet [liens vers Equipe], après la COP21. Une amie militante anglaise nous a présenté le Frackogram de Paul Mobbs sur le secteur des gaz de schiste au Royaume Uni [image]. Le coup de foudre avec l'idée a été immédiat. Il faisait tellement écho à des difficultés de compréhension et d'explication des réseaux tentaculaires d'influence auxquels nous, militants, avons tant de fois fait face. Comment expliquer clairement la puissance d'une entreprise comme Total ? Comment dévoiler l'étendue de l'industrie de l'évasion fiscale ? Le travail de Paul Mobbs nous rappelait la réponse : grâce à un bon dessin qui vaut (souvent) mieux qu'un long discours.

Après plusieurs mois de tâtonnement, et l'élargissement de l'équipe [lien vers Equipe], nous créons en novembre 2016 une association dont l'objectif est de mettre à disposition des acteurs de la transition écologique et sociale, des collectivités territoriales et des réseaux militants et alternatifs des outils de visualisation leur permettant de mieux comprendre et faire comprendre ce pour quoi (ou contre quoi) ils agissent, et de plus facilement partager des informations sur ces réseaux d'acteurs. 


Le premier  est Mycelia, une application open-source de visualisation de données. Plutôt que de faire les visualisations à la main, on s'est dit qu'un logiciel qui les génèrerait automatiquement à partir de données collectées via une interface simple (un formulaire par exemple), serait beaucoup plus efficace. Cette application, développée par Millicent, le développeur web génial et tout blanc d'AGATAs, permet de créer, à partir d'une interface collaborative, des graphs relationnels pour visualiser d'un coup d'œil des réseaux d'acteurs : quelles personnes physiques et morales s’y trouvent, quels liens entretiennent-eles, qu'échangent-elles...? A chaque acteur (ou "nœud" dans notre jargon) est associé une notice pour mieux le connaitre. Pour vous faire une idée, voici ci-dessous une démo du projet de visualisation de la toile d'influence de Total en France. Ce n'est qu'une démo ! On est en train de résoudre les bugs et améliorer la lisibilité de la visualisation. Nous préparons un prochain billet pour vous expliquer plus en détail les prochaines étapes de développement, les fonctionnalités envisagées, et les autres modes de visualisation imaginés.
 
En parallèle de Mycelia, nous souhaitons également mettre à disposition des outils d'animation pour utiliser l’application à plusieurs de façon collaborative, ou plus largement pour se former à la facilitation graphique, toujours selon l'idée qu'un bon dessin vaut mieux qu'un long discours. Ces outils se présenteront sous la forme de déroulé d'animation et d'exercices pratiques téléchargeables. 

AGATAs a un modèle économique hybride : nous construisons ces outils pour le bien commun, et nous réalisons des prestations, où nous mettons notre travail spécifiquement au service d'un client. Nous pouvons développer des fonctionnalités sur mesure, créer des visualisations à la demande, proposer des animations et des formations autour de la facilitation graphique... Nous allons bientôt mettre en ligne notre "catalogue" de prestations. Pourquoi ce modèle ? Parce qu'il nous parait être la meilleure manière de créer des outils fonctionnels et de les diffuser efficacement.

#### Et vous, dans tout ça ? 
Si vous aimez ce qu'on fait, vous pouvez tout simplement commencer par nous faire connaitre dans vos réseaux (surtout s'ils sont susceptibles d'aimer aussi). Mais surtout, vous pouvez nous rejoindre ! AGATAs, c'est aussi un projet associatif, où toutes les volontés sont les bienvenues. Il n'y a qu'une condition, c'est d'adhérer à notre charte des valeurs [lien], histoire d'être tous d'accord sur ce qu'on défend. A partir de là, tout est possible en fonction de vos envies et de vos compétences ! (et l'adhésion est à prix libre). 
Qui que vous soyez, vous pouvez participer à nos brainstorming pour imaginer des projets fous et sympathiques, nous aider à prioriser le développement de fonctionnalités en fonction de vos besoins, nous faire des retours sur un peu tout (bugs, communication, outils...), et bien d'autres choses !
Si vous faites de la traduction (anglais-français, français-espagnol, français-anglais, français-...peu importe, c'est cool dans tous les cas), on a des tonnes de manuels à vous passer, et tous nos contenus à traduire (parce qu'on pense à nos copains non-francophones). 
Si vous êtes une curieuse, un investigateur en herbe ou une journaliste expérimentée, et que vous voulez dévoiler avec nous les toiles d'influence des industriels sans scrupules et des élus corrompus, alors rejoignez nos projets de visualisation d'investigation !  
Si vous êtes développeur, hackeuse, informaticien, et vous aimeriez jeter un coup d'œil au code de Mycelia, le relire, l'améliorer même, le voici, le voila : [page Github]. Et si vous voulez nous aider à organiser des hackathons (ou autre), ramenez vos cheveux gras chez nous !
Si vous êtes plutôt communication, que vous aimez créer des pages d'accueil de site internet, ou faire des jolis visuels, organiser des campagnes de levées de fonds ou des évènements, alors ça tombe bien, on a besoin d'un coup de main pour tout ça ! 
Si vous êtes formateur en facilitation, animatrice éduc pop, que vous aimez vous creuser la tête sur des outils d'animation, et que vous aimez les supports visuels, c'est parfait, on est en train de développer notre boite à outils ! 
Si vous êtes juriste, comptable, gestionnaire expérimentée, entrepreneur chevronné, et que vous aimez donner des conseils, des infos, des tuyaux, notre porte est ouverte ! 
Et si ce sont nos prestations qui vous intéressent, nous serons ravi de vous proposer nos services ! 
Dans tous les cas : contactez-nous ! On a hâte de vous connaître. 

D'ailleurs, vous vous demandez peut être pourquoi le nom "AGATAs" ? C'est le nom contracté d'une araignée sociale, Angelena Consociata, vivant en communauté de milliers de membres, qui coopèrent sans structure hiérarchique pour attraper des proies dix fois plus grosses qu'elles. On a trouvé ça inspirant. 
Et Mycelia ? C'est le pluriel de mycélium, le réseau racinaire des champignons, qui peut à la fois être saprophyte (recycleur de déchets), symbiotique (transport et échange de nutriment avec les arbres), ou parasite. Un peu comme ce à quoi sert l'application : recycler des data en jolies visualisations, en symbiose avec les acteurs de la Transition, pour parasiter les parasites ! 

Alors on vous attend pour tisser notre toile racinaire ensemble ! 



